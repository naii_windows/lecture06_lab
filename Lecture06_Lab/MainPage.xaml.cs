﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Appointments;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Services.Maps;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Lecture06_Lab
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            MCMap.MapServiceToken = "NggniNfkWoAJYWaNrwu7~Ba_YkJv9SvsASrBV280AGQ~AqXz_H8d1GdioVSul1nTHcxPtsQlG3YdjJtPP9csVnPPyDNmc7kUv0G8x-QpQueG";
            
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            AddAppointment();
            CalcRoute();
        }

        public static Rect GetElementRect(FrameworkElement element)
        {
            GeneralTransform buttonTransform = element.TransformToVisual(null);
            Point point = buttonTransform.TransformPoint(new Point());
            return new Rect(point, new Size(element.ActualWidth, element.ActualHeight));
        }


        private async void AddAppointment()
        {
            var appointment = new Appointment();
            appointment.Subject = "Presentation Windows Apps";
            appointment.Details = "Give your presentation including a demo";
            appointment.StartTime = new DateTime(2018, 11, 7);
            appointment.Duration = TimeSpan.FromHours(4);

            var rect = MainPage.GetElementRect(this as FrameworkElement);

            String appointmentId = await AppointmentManager.
                ShowAddAppointmentAsync(appointment, rect, Windows.UI.Popups.Placement.Default);

        }

        private async void CalcRoute()
        {
            // Request permission to access location
            var accessStatus = await Geolocator.RequestAccessAsync();

            switch (accessStatus)
            {
                case GeolocationAccessStatus.Allowed:

                    // If DesiredAccuracy or DesiredAccuracyInMeters are not set (or value is 0), DesiredAccuracy.Default is used.
                    Geolocator geolocator = new Geolocator { DesiredAccuracyInMeters = 100 };

                    // Start at current position
                    Geoposition startPosition = await geolocator.GetGeopositionAsync().AsTask();

                    // End somewhere in Ghent center.
                    BasicGeoposition endLocation = new BasicGeoposition()
                    { Latitude = 51.050460, Longitude = 3.703510 };

                    
                    BasicGeoposition currentLocation = new BasicGeoposition();
                    currentLocation.Latitude = startPosition.Coordinate.Point.Position.Latitude;
                    currentLocation.Longitude = startPosition.Coordinate.Point.Position.Longitude;



                    Geopoint gp1 = new Geopoint(currentLocation);
                    Geopoint gp2 = new Geopoint(endLocation);

                    AddGeoPointToMap(gp1, "Start location");
                    AddGeoPointToMap(gp2, "End location");

                    ShowRouteOnMap(gp1, gp2);

                    break;

                case GeolocationAccessStatus.Denied:
                    //               
                    break;
            }
        }

        private async void ShowRouteOnMap(Geopoint gp1, Geopoint gp2)
        {
            // Get the route between the points.
            MapRouteFinderResult routeResult =
                  await MapRouteFinder.GetDrivingRouteAsync(
                  gp1,
                  gp2,
                  MapRouteOptimization.Time,
                  MapRouteRestrictions.None);

            if (routeResult.Status == MapRouteFinderStatus.Success)
            {
                // Use the route to initialize a MapRouteView.
                MapRouteView viewOfRoute = new MapRouteView(routeResult.Route);
                viewOfRoute.RouteColor = Colors.Yellow;
                viewOfRoute.OutlineColor = Colors.Black;

                // Add the new MapRouteView to the Routes collection
                // of the MapControl.
                MCMap.Routes.Add(viewOfRoute);

                // Fit the MapControl to the route.
                await MCMap.TrySetViewBoundsAsync(
                      routeResult.Route.BoundingBox,
                      null,
                      Windows.UI.Xaml.Controls.Maps.MapAnimationKind.None);
            }
        }

        private void AddGeoPointToMap(Geopoint gp, string title)
        {
            MapIcon mapIcon = new MapIcon();
            mapIcon.Location = gp;
            mapIcon.NormalizedAnchorPoint = new Point(0.5, 1.0);
            mapIcon.Title = title;
            mapIcon.ZIndex = 0;
            // Add the MapIcon to the map.
            MCMap.MapElements.Add(mapIcon);
        }
    }

}
